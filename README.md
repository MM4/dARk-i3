# dARK-i3 Config
## A simple i3-wm config working on Ubuntu 18.04

- Included a simple black top bar for i3-status bar and a black bottom bar for the workspaces
- Play / Pause `MOC` with mute button (My keyboard does not have music control button and I always listen to music)
- Launch MOC player by `Win + m`
- Lock computer by `Ctrl + l` with a black background.
- Control your clipboard with `Parcellite` in status bar.
- Seperate workspace for Firefox , Telegram , Sublime Text , Audacity , Music Player and even Terminal!
- Default key bindings for ease-of-work
- Shutdown shortcut availible: `Mod(Win key) + Shift + Ctrl + q`
- Keyboard volume control
- Using flameshot for default screenshot program
- Move floating windows to center by `Mod + C`
- monofur font for all environment
- Rofi for launching programs `Win + F2`
- annnnd much mooore...!! discover by reading the config file...

## Installation

You can easily run the `Install` script 
BUT if you want adventure... Let's begin !

### Requirements

For working the config without edit , you'll have to install some packages:
(Maybe these will be not correct ; anyway , sorry :) )

- `i3-wm`
- `i3-status`
- Other i3 packages if you like
- `rofi`
- `moc` MOC player for music on terminal
- `rxvt-unicode (To run MOC by default)`
- `flameshot` Screenshot helper
- `i3-lock` (Optional)
- `nm-tray`
- `feh`
- `parcellite`
- `compton` Optional
- `monofur` font (Included inside the `Font` Folder)
- `tilix` as default Terminal
- `dmenu` an alternative to rofi
- 
- Thats all!

### Config Installation

Copy the config folders inside the `Configs` folder into `~/.config/` folder.   
No other actions reqired.

### Font Installation

Copy monofur font inside `Font` folder into `~/.fonts` folder and execute the `fc-cache -f -v` command.


## Screenshots

They are inside the `Screenshots` folder.
